/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include <limits.h>
#include "LumiCalc/CoolQuery.h"
#include "RelationalAccess/ConnectionServiceException.h"

//===========================================================================
CoolQuery::CoolQuery(const std::string& database, const std::string& triggerchain): 
  m_repsort(0),
  m_database(database), 
  m_triggerchain(triggerchain),
  m_VKstart(0),
  m_VKstop(0),
  m_logger( "CoolQuery" ),
  m_valid(false),
  m_run3(false),
  m_smk_run(0),
  m_smk(0),
  m_trigDB(0),
  m_trigDBL1Pre(0),
  m_trigDBHLTPre(0)
{
  m_smk_l1id_map.clear();
  m_smk_seed_map.clear();
  m_l1pre_map.clear();
  m_hltpre_map.clear();
}

//===========================================================================
CoolQuery::~CoolQuery() {
  if ( m_sourceDbPtr.use_count()>0 && m_sourceDbPtr->isOpen() ) {
     m_logger << Root::kINFO << "Closing database '" << m_sourceDbPtr->databaseName() << Root::GEndl;
     m_sourceDbPtr->closeDatabase();
  }

}

//===========================================================================
bool CoolQuery::openDbConn() {

  m_logger << Root::kINFO << "Trying to connect to database " << m_database << "..." << Root::GEndl;

  //  CoraCoolDatabaseSvc& corasvc = CoraCoolDatabaseSvcFactory::databaseService();

  cool::IDatabaseSvc& databasesvc = cool::DatabaseSvcFactory::databaseService();
  //std::cout << "Done the CoraCool initialisation" << std::endl;
  //std::cout << "Opening CORAL database" << std::endl;
  try {
    m_repsort=new ReplicaSorter();
    coral::IConnectionServiceConfiguration& csconfig=m_coralsvc.configuration();
    csconfig.setReplicaSortingAlgorithm(*m_repsort);

    m_sourceDbPtr = databasesvc.openDatabase(m_database,true);// true --> readonly
    //    m_sourceCoraPtr=corasvc.openDatabase(m_database,true);// true --> readonly
    //std::cout << "....database connections open OK" << std::endl;
    return true;
   }
  catch (std::exception&e) {
    m_logger << Root::kERROR << "Problem opening CORAL database: " << e.what() << Root::GEndl;
    return false;
  }
  //  std::cout << "Done the database opening" << std::endl;
  
  // list the COOL folders found in the database

//    const std::vector<std::string>& folders=m_sourceDbPtr->listAllNodes();
//    std::cout << "COOL database has " << folders.size() << " folders defined"
//  	    << std::endl;
//    for (std::vector<std::string>::const_iterator itr=folders.begin();
//         itr!=folders.end();++itr) std::cout << *itr << std::endl;

  return false;

}

//===========================================================================
std::string CoolQuery::transConn(const std::string& inconn) {
  // translate simple connection string (no slash) to mycool.db with given                                                                                                              
  // instance name, all others are left alone                                                                                                                                           
  if (inconn.find('/')==std::string::npos) {
    return "sqlite://X;schema=mycool.db;dbname="+inconn;
  } else {
    return inconn;
  }
}

//===========================================================================
unsigned int CoolQuery::getTriggerLevel(const std::string& triggername){

  size_t found = triggername.find_first_of('_');
  if(found != std::string::npos){
    std::string s_lvl = triggername.substr(0,found);
    if(s_lvl == "EF") return 3;
    if(s_lvl == "L2") return 2;
    if(s_lvl == "L1") return 1;
    if(s_lvl == "HLT") return 2;
  }

  // Indicate no valid trigger name passed
  return 0;
    
}

//===========================================================================
void CoolQuery::setIOV(const cool::ValidityKey start, const cool::ValidityKey stop){
  m_VKstart = start;
  m_VKstop = stop;
}
  
void CoolQuery::setIOVForRun(unsigned int runnum) {
  cool::ValidityKey run = runnum;
  m_VKstart = (run << 32);
  m_VKstop = ((run+1) << 32) - 1;
}

void CoolQuery::ensureTrigDBLoaders() {

  // Utility to make sure all trigDB loaders are instantiated
  if (!m_trigDB) {
    m_logger << Root::kINFO << "Instantiate trigDB loader" << Root::GEndl;
    m_trigDB = new TrigConf::TrigDBMenuLoader("TRIGGERDB_RUN3");
    m_trigDB->setLevel(TrigConf::MSGTC::WARNING);
  }

  if (!m_trigDBL1Pre) {
    m_logger << Root::kINFO << "Instantiate trigDB L1 prescale loader" << Root::GEndl;
    m_trigDBL1Pre = new TrigConf::TrigDBL1PrescalesSetLoader("TRIGGERDB_RUN3");
    m_trigDBL1Pre->setLevel(TrigConf::MSGTC::WARNING);
  }

  if (!m_trigDBHLTPre) {
    m_logger << Root::kINFO << "Instantiate trigDB HLT prescale loader" << Root::GEndl;
    m_trigDBHLTPre = new TrigConf::TrigDBHLTPrescalesSetLoader("TRIGGERDB_RUN3");
    m_trigDBHLTPre->setLevel(TrigConf::MSGTC::WARNING);
  }

}

IOVData<float> 
CoolQuery::getL1PrescaleRun3(const std::string& trigger ) {

  IOVData<float> prescales;
  prescales.clear();

  if (trigger == "") return prescales;

  // Read L1 prescale keys
  IOVData<cool::UInt32> L1PSKObj 
    = getIOVData<cool::UInt32>("Lvl1PrescaleConfigurationKey", 
			       "/TRIGGER/LVL1/Lvl1ConfigKey", 0);

  // Set up trigDB if needed
  ensureTrigDBLoaders();

  for (auto it = L1PSKObj.data.begin(); it != L1PSKObj.data.end(); ++it) {

    IOVRange iov = it->first;
    cool::UInt32 l1psk = it->second;

    // Debugging output
    //m_logger << Root::kINFO << std::setw(1) << std::left << "[" << iov.start().event() << "," << iov.stop().event()-1 << "] L1PSK: " << l1psk << Root::GEndl;

    // Check if we have this cached
    if (m_l1pre_map.count(l1psk) && m_l1pre_map[l1psk].count(trigger)) {
      // Use cached value
      prescales.add( iov, m_l1pre_map[l1psk][trigger] );
      //m_logger << Root::kINFO << "Using cached value for L1 psk " << l1psk << " " << trigger << " -> " << m_l1pre_map[l1psk][trigger] << Root::GEndl;
    }
    else {
      // Read L1 prescales
      TrigConf::L1PrescalesSet l1pss;
      m_trigDBL1Pre->loadL1Prescales(l1psk, l1pss);
      auto pre = l1pss.prescale(trigger);  // Struct containing prescale info
      //m_logger << Root::kINFO << trigger << " L1PSK: " << l1psk << "-> cut: 0x" << std::hex << pre.cut << std::dec << " prescale: " << pre.prescale << " enabled: " << pre.enabled << Root::GEndl;
  
      if (pre.enabled) {
	prescales.add( iov, pre.prescale );
	m_l1pre_map[l1psk][trigger] = pre.prescale;
      } else {
	prescales.add( iov, -abs(pre.prescale) );  // Disabled triggers negative
	m_l1pre_map[l1psk][trigger] = -abs(pre.prescale);
      }
    }
  }

  return prescales;
}
 
IOVData<float> 
CoolQuery::getHLTPrescaleRun3(const std::string& trigger ) {

  IOVData<float> prescales;
  prescales.clear();

  if (trigger == "") return prescales;

  // Read HLT prescale keys
  IOVData<cool::UInt32> HLTPSKObj 
    = getIOVData<cool::UInt32>("HltPrescaleKey", 
			       "/TRIGGER/HLT/PrescaleKey", 0);

  // Set up trigDB if needed
  ensureTrigDBLoaders();

  for (auto it = HLTPSKObj.data.begin(); it != HLTPSKObj.data.end(); ++it) {

    IOVRange iov = it->first;
    cool::UInt32 hltpsk = it->second;

    // Debugging output
    // m_logger << Root::kINFO << std::left << "[" << iov.start().event() << "," << iov.stop().event()-1 << "] HLTPSK: " << hltpsk << Root::GEndl;

    // Check if we have this cached
    if (m_hltpre_map.count(hltpsk) && m_hltpre_map[hltpsk].count(trigger)) {
      // Use cached value
      prescales.add( iov, m_hltpre_map[hltpsk][trigger] );
      //m_logger << Root::kINFO << "Using cached value for HLT psk " << hltpsk << " " << trigger << " -> " << m_hltpre_map[hltpsk][trigger] << Root::GEndl;
    }
    else {
      // Read HLT prescales
      TrigConf::HLTPrescalesSet hltpss;
      m_trigDBHLTPre->loadHLTPrescales(hltpsk, hltpss);
      auto pre = hltpss.prescale(trigger);  // Struct containing prescale info
      //m_logger << Root::kINFO << trigger << " HLTPSK: " << hltpsk << " prescale: " << pre.prescale << " enabled: " << pre.enabled << Root::GEndl;

      if (pre.enabled) {  
	prescales.add( iov, pre.prescale );
	m_hltpre_map[hltpsk][trigger] = pre.prescale;
      } else {
	prescales.add( iov, -abs(pre.prescale) );
	m_hltpre_map[hltpsk][trigger] = -abs(pre.prescale);
      }

    }
  }

  return prescales;
}
 
cool::Int32 CoolQuery::getL1PrescaleFromChannelId(const std::string& folder_name, const cool::ChannelId& id ){

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  cool::IObjectIteratorPtr itr = folder_ptr->browseObjects(m_VKstart, m_VKstop,id);

  // Need to iterate once to get to first valid record, do it this way to avoid Coverity warning
  if (itr->goToNext()) {
    const cool::IRecord& payload=itr->currentRef().payload();    
    return payload["Lvl1Prescale"].data<cool::Int32>();
  }

  // Nonsense value
  return UINT_MAX;

}

cool::Float CoolQuery::getHLTPrescaleFromChannelId(const std::string& folder_name, const cool::ChannelId& id ){

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  cool::IObjectIteratorPtr itr = folder_ptr->browseObjects(m_VKstart, m_VKstop,id);

  // Need to iterate once to get to first valid record, do it this way to avoid Coverity warning
  if (itr->goToNext()) {
    const cool::IRecord& payload=itr->currentRef().payload();    
    return payload["Prescale"].data<cool::Float>();
  }

  // Nonsense value
  return -1.;

}

//===========================================================================
// Set SMK based on current IOV
unsigned int CoolQuery::setSMK() {

  // Check if run number hasn't changed
  unsigned int run = ( m_VKstart >> 32);
  if (run == m_smk_run) return m_smk;

  // Set dummy value
  m_smk = 0;
  m_smk_run = 0;

  // First we need to find the SMK
  IOVData<cool::UInt32> smk_data = getIOVData<cool::UInt32>("MasterConfigurationKey", "/TRIGGER/HLT/HltConfigKeys", 0);

  if (smk_data.size() == 0) {
    m_logger << Root::kERROR << "Can't find SMK for run " << run << "!" << Root::GEndl;
    return m_smk;
  }
  else if (smk_data.size() > 1) {
    m_logger << Root::kWARNING << "Found " << smk_data.size() << " SMK entries for run " << run << "!" << Root::GEndl;
  }

  // smk_data.data contians <IOV, data> pairs, get value of first element here
  m_smk = smk_data.data.front().second;
  m_logger << Root::kINFO << "Found SMK: " << m_smk << " for run " << run << Root::GEndl;
  m_smk_run = run;
  return m_smk;
}

//===========================================================================
cool::ChannelId CoolQuery::getL1ChannelId(const std::string& trigger, const std::string& folder_name){

  if (m_run3) {
    return getL1ChannelIdRun3(trigger);
  } else {
    return getL1ChannelIdOld(trigger, folder_name);
  }
}

//===========================================================================
// New (Run3) version where the L1 mapping is only in the TrigDB
cool::ChannelId CoolQuery::getL1ChannelIdRun3(const std::string& trigger){

  //m_logger << Root::kINFO << "Getting channel id for L1 trigger [" << trigger << "] from trigDB" << Root::GEndl;

  m_valid = false;

  cool::ChannelId chid = UINT_MAX;  // Nonesense value
  if (trigger == "") return chid;

  // Make sure smk is set
  if (!setSMK()) return chid;

  // See if this is cached
  if (m_smk_l1id_map.count(m_smk) && m_smk_l1id_map[m_smk].count(trigger)) {
    chid = m_smk_l1id_map[m_smk][trigger];
    //m_logger << Root::kINFO << "Returning cached value for SMK " << m_smk << " " << trigger << " -> " << chid << Root::GEndl;
    m_valid = true;
    return chid;
  }

  // Set up trigDB if needed
  ensureTrigDBLoaders();

  TrigConf::L1Menu l1_menu;
  try {
    m_trigDB->loadL1Menu( m_smk, l1_menu);
  }
  catch(coral::ReplicaNotAvailableException & ex) {
    m_logger << Root::kWARNING << "Error connecting to db " << ex.what() << " try again" << Root::GEndl;
    // Try again
    m_trigDB->loadL1Menu( m_smk, l1_menu);
  }

  TrigConf::L1Item item;
  try {
     item = l1_menu.item(trigger); 
  }
  catch(std::runtime_error & ex) {
    m_logger << Root::kWARNING << ex.what() << Root::GEndl;
    return chid;
  }

  chid = item.ctpId();
  m_logger << Root::kINFO << "Found " << trigger << " as " << chid << Root::GEndl;
  m_smk_l1id_map[m_smk][trigger] = chid;
  m_valid = true;

  return chid;
}

//===========================================================================
// Old (Run1/2) version where the L1 mapping was stored in COOL
cool::ChannelId CoolQuery::getL1ChannelIdOld(const std::string& trigger, const std::string& folder_name){

  //  m_logger << Root::kINFO << "Getting channel id for L1 trigger [" << trigger << "] from folder [" << folder_name << "]" << Root::GEndl;

  m_valid = false;

  if (trigger == "") return UINT_MAX;
 
  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  cool::IObjectIteratorPtr obj_itr=folder_ptr->browseObjects(m_VKstart,m_VKstart, cool::ChannelSelection::all());
  // loop through all triggers
  while (obj_itr->goToNext()){
    const cool::IRecord& payload=obj_itr->currentRef().payload();
    // find the L1 trigger chain
    if(payload["ItemName"].data<std::string>() == trigger){
      m_valid = true;
      //      cout << "Channel id: " << obj_itr->currentRef().channelId() << endl;
      return obj_itr->currentRef().channelId();
    }
  }

  if(!m_valid){
    m_logger << Root::kERROR << "Couldn't find L1 trigger [" << trigger << "] in folder [" << folder_name << "]" << Root::GEndl;
  }


  // Nonsense value
  return UINT_MAX;
}


//===========================================================================
cool::ChannelId CoolQuery::getLumiChannelId(const std::string& lumimethod, const std::string& folder_name){

  m_valid = false;

  if (lumimethod == "") return UINT_MAX;
 
  // m_logger << Root::kINFO << "Getting channel id for Lumi method: " << lumimethod << " in folder " << folder_name << Root::GEndl;

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  if(folder_ptr->existsChannel(lumimethod)){
    m_valid = true;
    return folder_ptr->channelId(lumimethod);
  }else{
    m_logger << Root::kWARNING << "Couldn't find lumimethod: " << lumimethod << " in COOL database!" << Root::GEndl;
  }

  // Nonsense value
  return UINT_MAX;
}

//===========================================================================
cool::ChannelId CoolQuery::getHLTChannelId(const std::string& trigger, const std::string& folder_name){

  // m_logger << Root::kINFO << "Getting channel id for HLT trigger [" << trigger << "] from folder [" << folder_name << "]" << Root::GEndl;

  m_valid = false;
  cool::ChannelId chan = UINT_MAX;

  if (trigger == "") return chan;

  // No need in Run3, but check that chain exists
  if (m_run3) {
    //m_logger << Root::kINFO << "getHLTChannelId called for Run3 data!" << Root::GEndl;

    // Just get lower channel ID, this will at worse cache the results 
    std::string seed = getHLTLowerChainName(trigger, "");
    if ( seed.size() > 0 ) {
      m_valid = true;
      chan = 0;
    }
    return chan;
  }

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  cool::IObjectIteratorPtr obj_itr=folder_ptr->browseObjects(m_VKstart,m_VKstart, cool::ChannelSelection::all());
  // loop through all triggers
  // loop through all triggers
  while (obj_itr->goToNext()){
    const cool::IRecord& payload=obj_itr->currentRef().payload();
    if(payload["ChainName"].data<std::string>() == trigger){
      m_valid = true;
      // m_logger << Root::kINFO << "Found channel id: " << payload["ChainCounter"].data<cool::UInt32>() << Root::GEndl;
      return payload["ChainCounter"].data<cool::UInt32>();
    }
  }
  
  if(!m_valid){
    m_logger << Root::kERROR << "Couldn't find HLT trigger [" << trigger << "] in folder [" << folder_name << "]" << Root::GEndl;
  }

  // Nonsense value
  return UINT_MAX;
}

void
CoolQuery::printL1Triggers(const std::string& folder_name) {

  if (m_run3) {
    m_logger << Root::kINFO << "Listing available triggers [triggername(chanid)]: " << Root::GEndl;

    // Make sure smk is set
    if (!setSMK()) {
      m_logger << Root::kWARNING << "Failed to find SMK in printL1Triggers!" << Root::GEndl;
      return;
    }

    ensureTrigDBLoaders();

    TrigConf::L1Menu l1_menu;
    m_trigDB->loadL1Menu( m_smk, l1_menu );
    for ( auto const& it : l1_menu ) {
      m_logger << Root::kINFO << it.name() << " (" << it.ctpId() << ")" << Root::GEndl;
    }

  } else {
    m_logger << Root::kINFO << "Listing available triggers [triggername(prescale, chanid)]: " << Root::GEndl;

    cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
    cool::IObjectIteratorPtr obj_itr=folder_ptr->browseObjects(m_VKstart,m_VKstart, cool::ChannelSelection::all());
    while (obj_itr->goToNext()){
      const cool::IRecord& payload=obj_itr->currentRef().payload();
      m_logger << Root::kINFO << payload["ItemName"].data<std::string>()  << " (" << this->getL1PrescaleFromChannelId("/TRIGGER/LVL1/Prescales",this->getL1ChannelId(payload["ItemName"].data<std::string>(), folder_name)) << ", " << obj_itr->currentRef().channelId() << "), ";
    }
    m_logger << Root::kINFO << Root::GEndl;
  }
}

void
CoolQuery::printHLTTriggers(const std::string& folder_name) {

  if (m_run3) {
    m_logger << Root::kINFO << "Listing available triggers [triggername]: " << Root::GEndl;

    // Make sure smk is set
    if (!setSMK()) {
      m_logger << Root::kWARNING << "Failed to find SMK in printHLTTriggers!" << Root::GEndl;
      return;
    }

    ensureTrigDBLoaders();

    TrigConf::HLTMenu hlt_menu;
    m_trigDB->loadHLTMenu( m_smk, hlt_menu );
    for ( auto const& it : hlt_menu ) {
      m_logger << Root::kINFO << it.name() << Root::GEndl;
    }

  } else {

    m_logger << Root::kINFO << "Listing available triggers [triggername(prescale, chanid)]: " << Root::GEndl;

    cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
    cool::IObjectIteratorPtr obj_itr2=folder_ptr->browseObjects(m_VKstart,m_VKstart, cool::ChannelSelection::all());
    while (obj_itr2->goToNext()){
      const cool::IRecord& payload2=obj_itr2->currentRef().payload();
      //      m_logger << Root::kINFO << payload2["ChainName"].data<std::string>()  << ", ";
      m_logger << Root::kINFO << payload2["ChainName"].data<std::string>()  << "(" << payload2["Prescale"].data<cool::Float>() << ", " << payload2["ChainCounter"].data<cool::UInt32>() << "), ";
    }
    m_logger << Root::kINFO << Root::GEndl;
  }
}

bool
CoolQuery::channelIdValid() {
  // m_logger << Root::kINFO << "channelIdValid = " << m_valid << Root::GEndl;
  return m_valid;
}

//===========================================================================
std::string CoolQuery::getHLTLowerChainName(const std::string& trigger, const std::string& folder_name){

  if (m_run3) {

    std::string seed("");

  // Make sure smk is set
    if (!setSMK()) return seed;

    // See if this is cached
    if (m_smk_seed_map.count(m_smk) && m_smk_seed_map[m_smk].count(trigger)) {
      seed = m_smk_seed_map[m_smk][trigger];
      m_logger << Root::kINFO << "Returning cached value for SMK " << m_smk << " " << trigger << " -> " << seed << Root::GEndl;
      return seed;
    }

    // Set up trigDB if needed
    ensureTrigDBLoaders();

    TrigConf::HLTMenu hlt_menu;
    try {
      m_trigDB->loadHLTMenu( m_smk, hlt_menu);
    }
    catch(coral::ReplicaNotAvailableException & ex) {
      m_logger << Root::kWARNING << "Error connecting to db " << ex.what() << " try again" << Root::GEndl;
      // Try again
      m_trigDB->loadHLTMenu( m_smk, hlt_menu);
    }

    // Search for HLT item swiped from RatesAnalysisAlg.cxx
    // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TrigCost/RatesAnalysis/src/RatesAnalysisAlg.cxx#0258

    TrigConf::HLTMenu::const_iterator chain = std::find_if(hlt_menu.begin(), hlt_menu.end(), [&] (const TrigConf::Chain& c) {return c.name() == trigger;});

    if (chain == hlt_menu.end()) {
      m_logger << Root::kERROR << "Couldn't find HLT trigger [" << trigger << "] in trigDB!" << Root::GEndl;
    } else {
      seed = (*chain).l1item();
      m_smk_seed_map[m_smk][trigger] = seed;
      m_logger << Root::kINFO << "Found " << trigger << " with L1 trigger " << seed << Root::GEndl;
    }
    return seed;
  }
  else {
    cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
    cool::IObjectIteratorPtr obj_itr=folder_ptr->browseObjects(m_VKstart,m_VKstart, cool::ChannelSelection::all());
    // loop through all triggers
    while (obj_itr->goToNext()){
      const cool::IRecord& payload=obj_itr->currentRef().payload();
      if(payload["ChainName"].data<std::string>() == trigger)
	return payload["LowerChainName"].data<std::string>();
    }

    m_logger << Root::kERROR << "Couldn't find HLT trigger [" << trigger << "] in folder [" << folder_name << "]" << Root::GEndl;

  }

  return "";

}

//===========================================================================
std::map<cool::ValidityKey, CoolQuery::LumiFolderData> 
CoolQuery::getLumiFolderData(const std::string& folder_name, const std::string& tag, const cool::ChannelId& id ){

  std::map<cool::ValidityKey, LumiFolderData> mymap;
  LumiFolderData folderData;

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  //  m_logger << Root::kWARNING << "Getting from database " << m_database << " tag " << tag << Root::GEndl; 
  if (!folder_ptr->existsChannel(id)) {
    m_logger << Root::kWARNING << "Lumi channel id " << id << " does not exist in database " << folder_name << "!" << Root::GEndl;
    return mymap;
  }

  cool::IObjectIteratorPtr itr;
  if(folder_ptr->existsUserTag(tag)) {
    itr = folder_ptr->browseObjects(m_VKstart, m_VKstop, id, tag);
  } else {
    // Try without specifying tag
    itr = folder_ptr->browseObjects(m_VKstart, m_VKstop, id);
  }

  while (itr->goToNext()) {
    const cool::IRecord& payload=itr->currentRef().payload();
    folderData.LBAvInstLumi = payload["LBAvInstLumi"].data<float>();
    folderData.LBAvEvtsPerBX = payload["LBAvEvtsPerBX"].data<float>();
    folderData.Valid = payload["Valid"].data<cool::UInt32>();
    mymap.insert( std::pair<cool::ValidityKey, LumiFolderData>(itr->currentRef().since(), folderData));
  }
  
  return mymap;

}

//===========================================================================
std::map<cool::ValidityKey, CoolQuery::L1CountFolderData> 
CoolQuery::getL1CountFolderData(const std::string& folder_name, const cool::ChannelId& id ){

  std::map<cool::ValidityKey, L1CountFolderData> mymap;
  L1CountFolderData folderData;

  cool::IFolderPtr folder_ptr = m_sourceDbPtr->getFolder(folder_name);
  //  m_logger << Root::kWARNING << "Getting from database " << m_database << " tag " << tag << Root::GEndl; 
  if (!folder_ptr->existsChannel(id)) {
    m_logger << Root::kWARNING << "Lumi channel id " << id << " does not exist in database " << folder_name << "!" << Root::GEndl;
    return mymap;
  }

  cool::IObjectIteratorPtr itr;

  itr = folder_ptr->browseObjects(m_VKstart, m_VKstop, id);

  while (itr->goToNext()) {
    const cool::IRecord& payload=itr->currentRef().payload();
    folderData.BeforePrescale = payload["BeforePrescale"].data<cool::UInt63>();
    folderData.AfterPrescale = payload["AfterPrescale"].data<cool::UInt63>();
    folderData.L1Accept = payload["L1Accept"].data<cool::UInt63>();
    mymap.insert( std::pair<cool::ValidityKey, L1CountFolderData>(itr->currentRef().since(), folderData));
  }
  
  return mymap;

}



